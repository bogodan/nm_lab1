#include <iostream>
#include <cmath>
#include <algorithm>
#include <functional>

double function (double x){
    return (std::pow(x,3) + sin(x) - 12*x +1);
}

double derivativeOfFunction(double x){
    return 3*x*x + cos(x) -12;
}

enum METHOD_TYPE{
    SIMPLE_ITERATION,
    NEWTON
};
double runIterationMethod(const std::function<double(double)>& function,std::pair<double,double> gap ,
                          double accuracy, METHOD_TYPE type){
    std::function<double(double)>convergent_function;
    if (type == SIMPLE_ITERATION){
        std::cout << "RUNNING SIMPLE ITERATION METHOD\n********************************" << std::endl;
        double lambda = derivativeOfFunction(gap.first) > derivativeOfFunction(gap.second)?
                        derivativeOfFunction(gap.first): derivativeOfFunction(gap.second);
        convergent_function = [lambda, function](double x) -> double{
            return x - function(x)/lambda;
        };
    }
    else if (type == NEWTON){
        std::cout << "RUNNING NEWTON METHOD\n**********************" << std::endl;
        convergent_function = [function](double x) -> double{
            return x - function(x)/ derivativeOfFunction(x);
        };
    }
    double x_prev = 0.5*(gap.first + gap.second);
    double x_next = x_prev;
    int iterations_count = 0;

    do{
        x_prev = x_next;
        x_next = convergent_function(x_prev);
        ++iterations_count;
        std::cout << "Iteration №" << iterations_count << " " <<  x_next << "\tf(x) = " << function(x_next)  << std::endl;
    }
    while (std::abs(x_next-x_prev) > accuracy );
    return x_next;
}




int main() {
    auto my_gap = std::make_pair(-4.,-3.);
    auto function = [](double x) -> double{
        return (std::pow(x,3) + sin(x) - 12*x +1);
    };
    double accuracy = 10E-4;
    double res = runIterationMethod(function, my_gap, accuracy,SIMPLE_ITERATION);
    std::cout << "result f(" << res << ") = " << function(res);

    return 0;
}
